# Example for usage of ingest auto for Marvel

## Goal test on hash verification

In samples/tuco_export.json data are present for Captain Marvel.

Movie Id is 2241.
With 2 Dcps of type TLR:
 * 3710 CaptainMarvel_TLR-B-3D_S_EN-fr_FR_51_2K_DI_20180922_DTB_IOP-3D
 * 3711 CaptainMarvel_TLR-B-2D_S_EN-fr_FR_51_2K_DI_20180923_DTB_IOP

## Prepare date

Here we simulate a machine for DCP ingest. We have to copy localy some DCPS.

Create a DCP storage location

```
mkdir -p $HOME/data/DCP
```

and copy the 2 previous DCP in this directory

## Env for ingestauto
in root dir of dcpingest-backend create a .env file and set the DCP_PATH

example
```
FLASK_ENV=development
FLASK_DEBUG=True
DCP_PATH=$HOME/data/DCP
```

## install tdcpbtools (dev version)
``` 
cd $HOME/dev/
git clone git@gitlab.com:indecp/tdcpbtools.git
```

go back to DCP ingest folder
```
cd cd $HOME/dev/dcpingest-backend
```
and update in the  the boostrap files, the PYTHONPATH with path to tdcpbtools project.

example in *bootstrap.sh* update the PYTHONPATH
```
export PYTHONPATH=$HOME/dev/tdcpbtools:$PYTHONPATH
```

same thing todo with  *bootstrap_worker.sh* :
```
export PYTHONPATH=$HOME/dev/tdcpbtools:$PYTHONPATH
```



## start dcpingest and worker for bacground tasks.

Long task in dcps ingest are managed asynchronously via a task manaqer and a queue. 
All that is managed by redis-queue.

So 2 processes shall be starter. Ideally create a screen (or tmux)
```
cd path/to/dcpingest-backend
screen -S dcpingest
. ./bootstrap.sh
flask run
```
Create a new terminal `[CTRL]+a than c` and start the worker
```
. ./bootstrap_worker.sh
```






