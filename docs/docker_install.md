## Prerequisists
```
apt-get install tmux python3-venv httpie
```


Add ssh key to gitlab / verify gitlab install

## install docker
https://docs.docker.com/install/linux/docker-ce/debian/

Ne pas oublier:
```
usermod -aG docker your-user
```
Tester: 
```
docker run hello-world
```


## Install docker compose: 
https://docs.docker.com/compose/install/

## Install dev version of tdcpbtools
```
mkdir -p $HOME/dev/ingestauto
cd $HOME/dev/ingestauto
git clone git@gitlab.com:indecp/tdcpbtools.git
cd tdcpbtools
python3 -m venv venv
. venv/bin/activate
pip install -r requirements.txt
```



## backend install
```
cd $HOME/dev/ingestauto
git clone git@gitlab.com:indecp/dcpingest-backend.git
cd dcpingest-backend
python3 -m venv venv
. venv/bin/activate
pip install -r requirements.txt
```

### Edit .env file

create .env file in $HOME/dev/ingestauto/dcpingest-backend

~~~
#FLASK
FLASK_ENV=development
FLASK_DEBUG=True

# DATABASE
INGEST_DATABASE=localingestdb
INGEST_DBUSER=ingestuser
INGEST_DBPASSWORD=XXXXXXXXXX

# REDIS
REDIS_URL=redis://@localhost:6399/0


# ingest auto data path
DCP_PATH=/data/tdcpb/downloads
TORRENT_DIR=/data/tdcpb/created_torrents
SEEDERS=user:password@IPTINC
~~~


in 'server' , 'worker' and 'ingscripts.sh' update PYTHONPATH with tdcpbtools

```
export PYTHONPATH=$HOME/dev/ingestauto/tdcpbtools:$PYTHONPATH
```

### Test docker compose

```
docker-compose up --build
```

**!!** Wait one minute for db reload: The db env variable are set one minute after. 
The mariadb restarts and env variables are set. No idea why : cf https://github.com/google/trillian/issues/1164

After one minute test delay connections: 

~~~
docker exec -ti dbingestauto mysql -u root -p
~~~
then verify the database is created:

~~~mysql
show databases;
~~~


### remove all container, images etc ...

~~~
docker-compose down --rmi all -v --remove-orphans

~~~

may be also
~~~
sudo systemctl restart docker
~~~

## run backend

```
./backend
```

### 1st run

Initialise db
```
./ingscripts.sh populate_local samples/local_ingest.json 
```

Test connection: 
```
 http http://localhost:5000/ingestapi/dcps
```


### install frontend

```
cd $HOME/dev/ingestauto
git clone git@gitlab.com:indecp/dcpingest-frontend.git
cd  dcpingest-frontend
npm install
npm install electron --save-dev
```


launch frontend
```
npm start
```


