# ingetasuto Distributors API

##  Base URL

Base URL is:
```
BASE_URL = http://localhost:port/ingestapi
```

### Get all distributors
* request = `GET BASE_URL/distributors`
* request parameters
* response:
  * return code: 200
  * payload : json | to be described
  * error : TBD
Example:
```
curl --header "Content-Type: application/json" --request GET http://localhost:5000/ingestapi/distributors
```
returns
```
[
    {
        "cnc_code": 1513,
        "user": 178,
        "contact": "",
        "isdcpbay": true,
        "movies": [
            1,
            59,
            84,
            ...,
            2199
        ],
        "name": "ARP SÉLECTION",
        "id": 1,
        "valid": true,
        "_links": {
            "self": "/ingestapi/distributors/1"
        }
    },
    {
        "cnc_code": 2908,
        "user": 151,
        "contact": "Jeanne Le Gall",
        "isdcpbay": true,
        "movies": [
            2,
            416,
            ...,
            2062
        ],
        "name": "ARIZONA FILMS",
        "id": 2,
        "valid": true,
        "_links": {
            "self": "/ingestapi/distributors/2"
        }
    },
    {
        "cnc_code": 1957,
        "user": 217,
        "contact": "",
        "isdcpbay": true,
        "movies": [
            3,
            20,
            23,
            ...,
            2235
        ],
        "name": "HAUT ET COURT",
        "id": 3,
        "valid": true,
        "_links": {
            "self": "/ingestapi/distributors/3"
        }
    },
...
]
```

### Get one distributor
* request = `GET BASE_URL/exhibitors/<id>`
* request parameters
* response:
  * return code: 200
  * payload : json | to be described
  * error : TBD
Example:
```
http GET localhost:5000/ingestapi/distributors/50
```
returns
```
HTTP/1.0 200 OK
Access-Control-Allow-Origin: *
Content-Length: 3585
Content-Type: application/json; charset=utf-8
Date: Fri, 18 Jan 2019 13:22:35 GMT
Server: Werkzeug/0.14.1 Python/3.5.3

{
    "_links": {
        "self": "/ingestapi/distributors/50"
    }, 
    "cnc_code": 1875, 
    "contact": "", 
    "id": 50, 
    "isdcpbay": true, 
    "movies": [
        {
            "_links": {
                "self": "/ingestapi/movies/82"
            }, 
            "id": 82, 
            "nb_valid_ftr": 1, 
            "nb_valid_tlr": 0, 
            "original_title": "LES MOOMINS SUR LA RIVIERA", 
            "releasedate": "2015-02-04", 
            "title": "LES MOOMINS SUR LA RIVIERA", 
            "valid": true
        }, 
        {
            "_links": {
                "self": "/ingestapi/movies/103"
            }, 
            "id": 103, 
            "nb_valid_ftr": 1, 
            "nb_valid_tlr": 0, 
            "original_title": "PANIQUE CHEZ LES JOUETS", 
            "releasedate": "2014-11-26", 
            "title": "PANIQUE CHEZ LES JOUETS", 
            "valid": true
        }, 
        {
            "_links": {
                "self": "/ingestapi/movies/109"
            }, 
            "id": 109, 
            "nb_valid_ftr": 1, 
            "nb_valid_tlr": 0, 
            "original_title": "EN SORTANT DE L'ÉCOLE", 
            "releasedate": "2014-10-01", 
            "title": "EN SORTANT DE L'ÉCOLE", 
            "valid": true
        }, 
        {
            "_links": {
                "self": "/ingestapi/movies/605"
            }, 
            "id": 605, 
            "nb_valid_ftr": 0, 
            "nb_valid_tlr": 0, 
            "original_title": "", 
            "releasedate": "2016-11-23", 
            "title": "LOUISE EN HIVER", 
            "valid": true
        }, 
        {
            "_links": {
                "self": "/ingestapi/movies/639"
            }, 
            "id": 639, 
            "nb_valid_ftr": 1, 
            "nb_valid_tlr": 1, 
            "original_title": "", 
            "releasedate": "2017-03-01", 
            "title": "PANIQUE TOUS COURTS", 
            "valid": true
        }, 
        {
            "_links": {
                "self": "/ingestapi/movies/697"
            }, 
            "id": 697, 
            "nb_valid_ftr": 1, 
            "nb_valid_tlr": 1, 
            "original_title": "", 
            "releasedate": "2017-04-05", 
            "title": "L'ÉCOLE DES LAPINS", 
            "valid": true
        }, 
        {
            "_links": {
                "self": "/ingestapi/movies/807"
            }, 
            "id": 807, 
            "nb_valid_ftr": 1, 
            "nb_valid_tlr": 1, 
            "original_title": "", 
            "releasedate": "2017-05-31", 
            "title": "LA CABANE À HISTOIRES", 
            "valid": true
        }, 
        {
            "_links": {
                "self": "/ingestapi/movies/1075"
            }, 
            "id": 1075, 
            "nb_valid_ftr": 1, 
            "nb_valid_tlr": 1, 
            "original_title": "", 
            "releasedate": "2017-10-18", 
            "title": "ZOMBILLENIUM", 
            "valid": true
        }, 
        {
            "_links": {
                "self": "/ingestapi/movies/1196"
            }, 
            "id": 1196, 
            "nb_valid_ftr": 1, 
            "nb_valid_tlr": 1, 
            "original_title": "", 
            "releasedate": "2018-02-07", 
            "title": "RITA ET CROCODILE", 
            "valid": true
        }, 
        {
            "_links": {
                "self": "/ingestapi/movies/1207"
            }, 
            "id": 1207, 
            "nb_valid_ftr": 0, 
            "nb_valid_tlr": 0, 
            "original_title": "", 
            "releasedate": "2016-10-19", 
            "title": "MA VIE DE COURGETTE", 
            "valid": true
        }, 
        {
            "_links": {
                "self": "/ingestapi/movies/1276"
            }, 
            "id": 1276, 
            "nb_valid_ftr": 1, 
            "nb_valid_tlr": 1, 
            "original_title": "", 
            "releasedate": "2017-12-13", 
            "title": "DRÔLES DE PETITES BÊTES", 
            "valid": true
        }, 
        {
            "_links": {
                "self": "/ingestapi/movies/1448"
            }, 
            "id": 1448, 
            "nb_valid_ftr": 1, 
            "nb_valid_tlr": 1, 
            "original_title": "", 
            "releasedate": "2018-03-07", 
            "title": "LIBERTÉ  13 FILMS-POÈMES DE PAUL ELUARD", 
            "valid": true
        }, 
        {
            "_links": {
                "self": "/ingestapi/movies/2066"
            }, 
            "id": 2066, 
            "nb_valid_ftr": 0, 
            "nb_valid_tlr": 0, 
            "original_title": "", 
            "releasedate": "2019-01-23", 
            "title": "ANOTHER DAY OF LIFE", 
            "valid": true
        }, 
        {
            "_links": {
                "self": "/ingestapi/movies/2105"
            }, 
            "id": 2105, 
            "nb_valid_ftr": 0, 
            "nb_valid_tlr": 1, 
            "original_title": "", 
            "releasedate": "2018-11-21", 
            "title": "L'ENFANCE D'UN MAÎTRE", 
            "valid": true
        }, 
        {
            "_links": {
                "self": "/ingestapi/movies/2106"
            }, 
            "id": 2106, 
            "nb_valid_ftr": 1, 
            "nb_valid_tlr": 1, 
            "original_title": "", 
            "releasedate": "2018-12-26", 
            "title": "KIRIKOU ET LA SORCIÈRE", 
            "valid": true
        }
    ], 
    "name": "GEBEKA FILMS", 
    "user": {
        "_links": {
            "self": "/ingestapi/users/194"
        }, 
        "about_me": null, 
        "distributor": {
            "_links": {
                "self": "/ingestapi/distributors/50"
            }, 
            "cnc_code": 1875, 
            "contact": "", 
            "id": 50, 
            "isdcpbay": true, 
            "name": "GEBEKA FILMS", 
            "valid": true
        }, 
        "email": "info@gebekafilms.com", 
        "exhibitor": null, 
        "id": 194, 
        "login": "gebekafilms", 
        "role": "distributor"
    }, 
    "valid": true
}

```

### Get movies from distributor
* request = `GET BASE_URL/exhibitors/<id>/movies`
* request parameters
* response:
  * return code: 200
  * payload : json | to be described
  * error : TBD
  * sorted by release date

Example:
```
http GET localhost:5000/ingestapi/distributors/50/movies/

```

returns:

```

    {
        "_links": {
            "self": "/ingestapi/movies/2066"
        }, 
        "dcps": [], 
        "distributor": 50, 
        "id": 2066, 
        "nb_valid_ftr": 0, 
        "nb_valid_tlr": 0, 
        "original_title": "", 
        "releasedate": "2019-01-23", 
        "title": "ANOTHER DAY OF LIFE", 
        "valid": true
    }, 
    {
        "_links": {
            "self": "/ingestapi/movies/2106"
        }, 
        "dcps": [
            {
                "_links": {
                    "collection": "/ingestapi/dcps", 
                    "self": "/ingestapi/dcps/3459", 
                    "short_check_status": "/ingestapi/dcps/3459/short_check_status"
                }, 
                "contentkind": "TLR", 
                "hash_verification_date": null, 
                "hash_verification_state_name": "NOT_DONE", 
                "hash_verification_state_value": 1, 
                "id": 3459, 
                "ingest_dcp_date": null, 
                "ingest_dcp_state_name": "NOT_DONE", 
                "ingest_dcp_state_value": 1, 
                "movie": 2106, 
                "name": "KIRIKOU_TLR_F_FR-XX_FR_51_2K_20180910_LUM_IOP_OV", 
                "size": 1506663502, 
                "tasks": [], 
                "torrent_creation_date": "2018-10-01T15:23:45+00:00", 
                "torrent_creation_state_name": "NOT_DONE", 
                "torrent_creation_state_value": 1, 
                "torrent_hash": "922f02a65a1d0ba9e1b44f5423422a82c7d8d482", 
                "valid": true
            }, 
            {
                "_links": {
                    "collection": "/ingestapi/dcps", 
                    "self": "/ingestapi/dcps/3673", 
                    "short_check_status": "/ingestapi/dcps/3673/short_check_status"
                }, 
                "contentkind": "FTR", 
                "hash_verification_date": null, 
                "hash_verification_state_name": "NOT_DONE", 
                "hash_verification_state_value": 1, 
                "id": 3673, 
                "ingest_dcp_date": null, 
                "ingest_dcp_state_name": "NOT_DONE", 
                "ingest_dcp_state_value": 1, 
                "movie": 2106, 
                "name": "KIRIKOU-ET-LA-SORCIERE_FTR_F_FR-XX_FR_51_2K_GBK_20120803_ECL_OV", 
                "size": 101063905294, 
                "tasks": [], 
                "torrent_creation_date": "2018-11-27T12:41:00+00:00", 
                "torrent_creation_state_name": "NOT_DONE", 
                "torrent_creation_state_value": 1, 
                "torrent_hash": "f60644ab87412b3cfaefa2ce3dbc23d01a0570dd", 
                "valid": true
            }
        ], 
        "distributor": 50, 
        "id": 2106, 
        "nb_valid_ftr": 1, 
        "nb_valid_tlr": 1, 
        "original_title": "", 
        "releasedate": "2018-12-26", 
        "title": "KIRIKOU ET LA SORCIÈRE", 
        "valid": true
    }, 

....
]

```
returns

### Get newest movies from distributor
* request = `GET BASE_URL/exhibitors/<id>/movies`
* request parameters
* response:
  * return code: 200
  * payload : json | to be described
  * error : TBD
  * sorted by release date . limited to 5 movies.

Example:
```
http GET localhost:5000/ingestapi/distributors/50/movies_newest
```
returns

```
 ~ http GET localhost:5000/ingestapi/distributors/50/movies_newest
HTTP/1.0 200 OK
Access-Control-Allow-Origin: *
Content-Length: 6112
Content-Type: application/json; charset=utf-8
Date: Fri, 18 Jan 2019 13:36:25 GMT
Server: Werkzeug/0.14.1 Python/3.5.3

[
    {
        "_links": {
            "self": "/ingestapi/movies/2066"
        }, 
        "dcps": [], 
        "distributor": 50, 
        "id": 2066, 
        "nb_valid_ftr": 0, 
        "nb_valid_tlr": 0, 
        "original_title": "", 
        "releasedate": "2019-01-23", 
        "title": "ANOTHER DAY OF LIFE", 
        "valid": true
    }, 
    {
        "_links": {
            "self": "/ingestapi/movies/2106"
        }, 
        "dcps": [
            {
                "_links": {
                    "collection": "/ingestapi/dcps", 
                    "self": "/ingestapi/dcps/3459", 
                    "short_check_status": "/ingestapi/dcps/3459/short_check_status"
                }, 
                "contentkind": "TLR", 
                "hash_verification_date": null, 
                "hash_verification_state_name": "NOT_DONE", 
                "hash_verification_state_value": 1, 
                "id": 3459, 
                "ingest_dcp_date": null, 
                "ingest_dcp_state_name": "NOT_DONE", 
                "ingest_dcp_state_value": 1, 
                "movie": 2106, 
                "name": "KIRIKOU_TLR_F_FR-XX_FR_51_2K_20180910_LUM_IOP_OV", 
                "size": 1506663502, 
                "tasks": [], 
                "torrent_creation_date": "2018-10-01T15:23:45+00:00", 
                "torrent_creation_state_name": "NOT_DONE", 
                "torrent_creation_state_value": 1, 
                "torrent_hash": "922f02a65a1d0ba9e1b44f5423422a82c7d8d482", 
                "valid": true
            }, 
            {
                "_links": {
                    "collection": "/ingestapi/dcps", 
                    "self": "/ingestapi/dcps/3673", 
                    "short_check_status": "/ingestapi/dcps/3673/short_check_status"
                }, 
                "contentkind": "FTR", 
                "hash_verification_date": null, 
                "hash_verification_state_name": "NOT_DONE", 
                "hash_verification_state_value": 1, 
                "id": 3673, 
                "ingest_dcp_date": null, 
                "ingest_dcp_state_name": "NOT_DONE", 
                "ingest_dcp_state_value": 1, 
                "movie": 2106, 
                "name": "KIRIKOU-ET-LA-SORCIERE_FTR_F_FR-XX_FR_51_2K_GBK_20120803_ECL_OV", 
                "size": 101063905294, 
                "tasks": [], 
                "torrent_creation_date": "2018-11-27T12:41:00+00:00", 
                "torrent_creation_state_name": "NOT_DONE", 
                "torrent_creation_state_value": 1, 
                "torrent_hash": "f60644ab87412b3cfaefa2ce3dbc23d01a0570dd", 
                "valid": true
            }
        ], 
        "distributor": 50, 
        "id": 2106, 
        "nb_valid_ftr": 1, 
        "nb_valid_tlr": 1, 
        "original_title": "", 
        "releasedate": "2018-12-26", 
        "title": "KIRIKOU ET LA SORCIÈRE", 
        "valid": true
    }, 
    {
        "_links": {
            "self": "/ingestapi/movies/2105"
        }, 
        "dcps": [
            {
                "_links": {
                    "collection": "/ingestapi/dcps", 
                    "self": "/ingestapi/dcps/3458", 
                    "short_check_status": "/ingestapi/dcps/3458/short_check_status"
                }, 
                "contentkind": "TLR", 
                "hash_verification_date": null, 
                "hash_verification_state_name": "NOT_DONE", 
                "hash_verification_state_value": 1, 
                "id": 3458, 
                "ingest_dcp_date": null, 
                "ingest_dcp_state_name": "NOT_DONE", 
                "ingest_dcp_state_value": 1, 
                "movie": 2105, 
                "name": "ENFANCEDUNMAIT_TLR_F_FR-fr_51_2K_MLP_20180913_LUM_IOP", 
                "size": 1326514841, 
                "tasks": [], 
                "torrent_creation_date": "2018-10-01T15:23:34+00:00", 
                "torrent_creation_state_name": "NOT_DONE", 
                "torrent_creation_state_value": 1, 
                "torrent_hash": "e7a397dbf40f54c6dbc424b6a853a293747d81f1", 
                "valid": true
            }
        ], 
        "distributor": 50, 
        "id": 2105, 
        "nb_valid_ftr": 0, 
        "nb_valid_tlr": 1, 
        "original_title": "", 
        "releasedate": "2018-11-21", 
        "title": "L'ENFANCE D'UN MAÎTRE", 
        "valid": true
    }, 
    {
        "_links": {
            "self": "/ingestapi/movies/1448"
        }, 
        "dcps": [
            {
                "_links": {
                    "collection": "/ingestapi/dcps", 
                    "self": "/ingestapi/dcps/2574", 
                    "short_check_status": "/ingestapi/dcps/2574/short_check_status"
                }, 
                "contentkind": "TLR", 
                "hash_verification_date": null, 
                "hash_verification_state_name": "NOT_DONE", 
                "hash_verification_state_value": 1, 
                "id": 2574, 
                "ingest_dcp_date": null, 
                "ingest_dcp_state_name": "NOT_DONE", 
                "ingest_dcp_state_value": 1, 
                "movie": 1448, 
                "name": "ELUARD_LIBERTE_TLR_F-185_20_20180119_SMPTE", 
                "size": 896672254, 
                "tasks": [], 
                "torrent_creation_date": "2018-01-26T12:10:52+00:00", 
                "torrent_creation_state_name": "NOT_DONE", 
                "torrent_creation_state_value": 1, 
                "torrent_hash": "b3adec89c7af536f150014a9cb3d58987d3044d3", 
                "valid": true
            }, 
            {
                "_links": {
                    "collection": "/ingestapi/dcps", 
                    "self": "/ingestapi/dcps/2582", 
                    "short_check_status": "/ingestapi/dcps/2582/short_check_status"
                }, 
                "contentkind": "FTR", 
                "hash_verification_date": null, 
                "hash_verification_state_name": "NOT_DONE", 
                "hash_verification_state_value": 1, 
                "id": 2582, 
                "ingest_dcp_date": null, 
                "ingest_dcp_state_name": "NOT_DONE", 
                "ingest_dcp_state_value": 1, 
                "movie": 1448, 
                "name": "GEBEKA_EN-SORTANT-ECOLE_S04_FTR-25_2K-F-177_51FR_20180103_SMPTE_1801TVS0040", 
                "size": 40561018863, 
                "tasks": [], 
                "torrent_creation_date": "2018-01-29T11:26:55+00:00", 
                "torrent_creation_state_name": "NOT_DONE", 
                "torrent_creation_state_value": 1, 
                "torrent_hash": "f255f95d6cccdc413433caeafbcf33d3edc9f9c4", 
                "valid": true
            }
        ], 
        "distributor": 50, 
        "id": 1448, 
        "nb_valid_ftr": 1, 
        "nb_valid_tlr": 1, 
        "original_title": "", 
        "releasedate": "2018-03-07", 
        "title": "LIBERTÉ  13 FILMS-POÈMES DE PAUL ELUARD", 
        "valid": true
    }, 
    {
        "_links": {
            "self": "/ingestapi/movies/1196"
        }, 
        "dcps": [
            {
                "_links": {
                    "collection": "/ingestapi/dcps", 
                    "self": "/ingestapi/dcps/2193", 
                    "short_check_status": "/ingestapi/dcps/2193/short_check_status"
                }, 
                "contentkind": "TLR", 
                "hash_verification_date": null, 
                "hash_verification_state_name": "NOT_DONE", 
                "hash_verification_state_value": 1, 
                "id": 2193, 
                "ingest_dcp_date": null, 
                "ingest_dcp_state_name": "NOT_DONE", 
                "ingest_dcp_state_value": 1, 
                "movie": 1196, 
                "name": "RITA-ET-CROCODILE_TLR_F_FR-XX_FR_51_2K_20170906_LUM_IOP_OV", 
                "size": 987478610, 
                "tasks": [], 
                "torrent_creation_date": "2017-10-12T13:51:50+00:00", 
                "torrent_creation_state_name": "NOT_DONE", 
                "torrent_creation_state_value": 1, 
                "torrent_hash": "852fb5575a2d15a0f4808c56f01201de1f33346b", 
                "valid": true
            }, 
            {
                "_links": {
                    "collection": "/ingestapi/dcps", 
                    "self": "/ingestapi/dcps/2201", 
                    "short_check_status": "/ingestapi/dcps/2201/short_check_status"
                }, 
                "contentkind": "FTR", 
                "hash_verification_date": null, 
                "hash_verification_state_name": "NOT_DONE", 
                "hash_verification_state_value": 1, 
                "id": 2201, 
                "ingest_dcp_date": null, 
                "ingest_dcp_state_name": "NOT_DONE", 
                "ingest_dcp_state_value": 1, 
                "movie": 1196, 
                "name": "RitaCrocodile_FTR-25_F-177_FR-XX_FR_51_2K_GBK_20170918_ECL_IOP_OV", 
                "size": 48659039894, 
                "tasks": [], 
                "torrent_creation_date": "2017-10-13T20:15:42+00:00", 
                "torrent_creation_state_name": "NOT_DONE", 
                "torrent_creation_state_value": 1, 
                "torrent_hash": "fef5d279916eea1f45393fa701601c9479d1548f", 
                "valid": true
            }
        ], 
        "distributor": 50, 
        "id": 1196, 
        "nb_valid_ftr": 1, 
        "nb_valid_tlr": 1, 
        "original_title": "", 
        "releasedate": "2018-02-07", 
        "title": "RITA ET CROCODILE", 
        "valid": true
    }
]

```

