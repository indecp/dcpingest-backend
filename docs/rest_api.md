# ingest auto backend REST API

##  Base URL

Base URL is:
```
BASE_URL = http://localhost:port/ingestapi
```

## Authentication
To be implemented

## DCP API
[ Distributors API  documentation](docs/rest_api_distributors.md)


### Get all exhibitors
* request = `GET BASE_URL/exhibitors`
* request parameters
* response:
  * return code: 200
  * payload : json | to be described
  * error : TBD
Example:
```
curl --header "Content-Type: application/json" --request GET http://localhost:5000/ingestapi/exhibitors
```
returns
```

[
    {
        "user": 1,
        "cncid": null,
        "dept": 33,
        "name": "Utopia Saint-Simeon",
        "valid": true,
        "id": 1,
        "contact": "",
        "iptinc": "10.10.10.33",
        "zipcode": "",
        "nbscreens": 5,
        "phone": null,
        "city": "BORDEAUX",
        "address": "5, place Camille-Jullian",
        "_links": {
            "self": "/ingestapi/exhibitors/1"
        }
    },
    {
        "user": 2,
        "cncid": null,
        "dept": 31,
        "name": "American Cosmograph (ex Utopia)",
        "valid": true,
        "id": 2,
        "contact": "Fred",
        "iptinc": "10.10.10.131",
        "zipcode": "",
        "nbscreens": 3,
        "phone": null,
        "city": "TOULOUSE",
        "address": "24, Rue Montardy ",
        "_links": {
            "self": "/ingestapi/exhibitors/2"
        }
    },
    ....
]
```
### Get one exhibitor
* request = `GET BASE_URL/exhibitors/<id>`
* request parameters
* response:
  * return code: 200
  * payload : json | to be described
  * error : TBD
Example:
```
curl --header "Content-Type: application/json" --request GET http://localhost:5000/ingestapi/exhibitors/4
```
returns
```
{
    "user": 4,
    "cncid": null,
    "dept": 34,
    "name": "Utopia Sainte-Bernadette",
    "valid": true,
    "id": 4,
    "contact": "Arnaud CLAPPIER",
    "iptinc": "10.10.10.34",
    "zipcode": "",
    "nbscreens": 3,
    "phone": null,
    "city": "MONTPELLIER",
    "address": "5, Avenue du Dr Pezet",
    "_links": {
        "self": "/ingestapi/exhibitors/4"
    }
}
```


### Get all DCPs

* request = `GET BASE_URL/dcps`
* request parameters
  * pageNumber: default = 1
  * pageSize: default = 50
  * filter: default = null
  * sort : TBD
* response:
  * return code: 200
  * payload : json | to be described
  * error : TBD

### Get one DCP
* request = `GET BASE_URL/dcps/<id>`
* request parameters
  * null
* response:
  * return code: 200
  * payload : json | to be described
  * error : TBD

### Add DCP
* request = `POST BASE_URL/dcps`
* request parameters
  * ingest : dafault = false
  * dcp_path: defautl = null
* response:
  * return code: 201(Created) if no ingest else 202(Accepted)
  * payload : json | to be described
  * error : TBD


### Do Dcp verification
* request = `POST BASE_URL/dcps/<id>/do_check_long`
* request parameters: none
* response:
  * return code: 202(Accepted)
  * payload : json | to be described
  * error : TBD



