import base64
from enum import Enum, IntEnum
from datetime import datetime, timedelta
from hashlib import md5
import json
import os
from time import time
from flask import current_app, url_for
from werkzeug.security import generate_password_hash, check_password_hash
import redis
import rq
from app import db
from app import ma
from marshmallow import fields
from app.search import add_to_index, remove_from_index, query_index




class DcpOperationState(Enum):
    N_A=0,
    NOT_DONE=1
    ON_PROGRESS=2
    OK=3
    FAIL=4
    PENDING=5

class UserRole(IntEnum):
    exhibitor = 1
    distributor =2
    ingester=3
    useapi = 4
    admin = 255



class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(64),  unique=True)
    email = db.Column(db.String(120), unique=True)
    role          = db.Column(db.Integer)
    password_hash = db.Column(db.String(128))
    about_me = db.Column(db.String(140))
    last_seen = db.Column(db.DateTime, default=datetime.utcnow)
    token = db.Column(db.String(32), index=True, unique=True)
    token_expiration = db.Column(db.DateTime)
    tasks = db.relationship('Task', backref='user', lazy='dynamic')
    distributor   = db.relationship('Distributor',
                                              uselist=False, backref="user")
    exhibitor   = db.relationship('Exhibitor',
                                              uselist=False, backref="user")
    notifications = db.relationship('Notification', backref='user',
                                    lazy='dynamic')

    def __repr__(self):
        return '<User {}>'.format(self.login)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def avatar(self, size):
        digest = md5(self.email.lower().encode('utf-8')).hexdigest()
        return 'https://www.gravatar.com/avatar/{}?d=identicon&s={}'.format(
            digest, size)

    def new_messages(self):
        last_read_time = self.last_message_read_time or datetime(1900, 1, 1)
        return Message.query.filter_by(recipient=self).filter(
            Message.timestamp > last_read_time).count()

    def add_notification(self, name, data):
        self.notifications.filter_by(name=name).delete()
        n = Notification(name=name, payload_json=json.dumps(data), user=self)
        db.session.add(n)
        return n

    def launch_task(self, name, description, *args, **kwargs):
        print ("args:",args)
        print ("kwargs:",kwargs)
        rq_job = current_app.task_queue.enqueue('app.tasks.' + name, self.id,
                                                timeout = '12h',
                                                result_ttl= -1,
                                                *args, **kwargs)
        task = Task(id=rq_job.get_id(), name=name, description=description,
                    user=self)
        db.session.add(task)
        db.session.commit()
        return rq_job

    def get_tasks_in_progress(self):
        return Task.query.filter_by(user=self, complete=False).all()

    def get_task_in_progress(self, name):
        return Task.query.filter_by(name=name, user=self,
                                    complete=False).first()

    def to_dict(self, include_email=False):
        data = {
            'id': self.id,
            'login': self.login,
            'last_seen': self.last_seen.isoformat() + 'Z',
            'about_me': self.about_me,
            'post_count': self.posts.count(),
            '_links': {
                'self': url_for('api.get_user', id=self.id),
                'avatar': self.avatar(128)
            }
        }
        if include_email:
            data['email'] = self.email
        return data

    def from_dict(self, data, new_user=False):
        for field in ['login', 'email', 'about_me']:
            if field in data:
                setattr(self, field, data[field])
        if new_user and 'password' in data:
            self.set_password(data['password'])

    def get_token(self, expires_in=3600):
        now = datetime.utcnow()
        if self.token and self.token_expiration > now + timedelta(seconds=60):
            return self.token
        self.token = base64.b64encode(os.urandom(24)).decode('utf-8')
        self.token_expiration = now + timedelta(seconds=expires_in)
        db.session.add(self)
        return self.token

    def revoke_token(self):
        self.token_expiration = datetime.utcnow() - timedelta(seconds=1)

    @staticmethod
    def check_token(token):
        user = User.query.filter_by(token=token).first()
        if user is None or user.token_expiration < datetime.utcnow():
            return None
        return user

class Distributor(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    cnc_code = db.Column(db.Integer)
    contact = db.Column(db.String(140))
    name = db.Column(db.String(140))
    isdcpbay = db.Column(db.Boolean)
    valid  = db.Column(db.Boolean, default=True)
    userid = db.Column(db.Integer, db.ForeignKey('user.id'))
    movies = db.relationship('Movie', backref=db.backref('distributor', lazy="joined"), lazy='dynamic')

    def __repr__(self):
        return '<Distributor {}>'.format(self.name)

    def from_dict(self, data):
        for field in ['valid', 'name', 'isdcpbay', 'cnc_code', 'contact']:
            if field in data:
                setattr(self, field, data[field])

class Exhibitor(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    valid  = db.Column(db.Boolean, default=True)
    cncid = db.Column(db.Integer)
    name = db.Column(db.String(140))
    address = db.Column(db.String(200))
    contact = db.Column(db.String(120))
    city = db.Column(db.String(200))
    dept = db.Column(db.Integer)
    phone = db.Column(db.String(40))
    zipcode = db.Column(db.String(20))
    nbscreens = db.Column(db.Integer)
    iptinc = db.Column(db.String(15), unique=True)
    userid = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __repr__(self):
        return '<Exhibitor {}>'.format(self.name)

    def from_dict(self, data):
        for field in ['valid', 'name', 'address', 'contact', 'city', 'dept',
                      'zipcode', 'nbscreens', 'iptinc']:
            if field in data:
                setattr(self, field, data[field])




class Movie(db.Model):

    id             = db.Column(db.Integer, primary_key=True)
    valid          = db.Column(db.Boolean, default=True)
    title          = db.Column(db.Text)
    original_title = db.Column(db.Text)
    releasedate    = db.Column(db.Date)
    synopsis       = db.Column(db.Text)
    director       = db.Column(db.Text)
    actor          = db.Column(db.Text)
    country        = db.Column(db.Text)
    scenario       = db.Column(db.Text)
    nb_valid_ftr   = db.Column(db.Integer)
    nb_valid_tlr   = db.Column(db.Integer)
    distributorid =  db.Column(db.Integer, db.ForeignKey('distributor.id'))

    def to_dict(self):
        data = {
            'id': self.id,
            'valid': self.valid,
            'title': self.title,
            'original_title': self.original_title,
            }
        data['dcps']=[]
        for dcp in self.dcps.all():
            data['dcps'].append(dcp.name)
        return data

    def from_dict(self, data):
        for field in ['valid', 'title', 'original_title', 'releasedate',
                      'synopsis', 'director', 'actor', 'counrty', 'scenario']:
            if field in data:
                setattr(self, field, data[field])




class Dcp(db.Model):
    # types
    TRAILER = "TLR"
    FEATURE = "FTR"
    SHORT   = "SHR"

    id                = db.Column(db.Integer, primary_key=True)
    valid             = db.Column(db.Boolean, default=True)
    on_seed           = db.Column(db.Boolean, default=False)
    on_seed_active    = db.Column(db.Boolean, default=False)
    on_seed_date      = db.Column(db.DateTime)
    encrypted         = db.Column(db.Boolean)
    name              = db.Column(db.String(150), unique = True,
                                  nullable = False)
    contentkind       = db.Column(db.String(5))
    size              = db.Column(db.BigInteger)
    torrent_hash      = db.Column(db.String(40))
    ingest_dcp_state_value = db.Column(db.Integer, default =
                                       int(DcpOperationState.NOT_DONE.value))
    ingest_dcp_state_name  = db.Column(db.String(40), default =
                                       str(DcpOperationState.NOT_DONE.name))
    ingest_dcp_date        = db.Column(db.DateTime)
    hash_verification_state_value = db.Column(db.Integer,
                                              default
                                              =int(DcpOperationState.NOT_DONE.value))
    hash_verification_state_name = db.Column(
        db.String(40),
        default =str(DcpOperationState.NOT_DONE.name))
    hash_verification_date        = db.Column(db.DateTime)
    torrent_creation_state_value = db.Column(db.Integer, default =
                                             int(DcpOperationState.NOT_DONE.value))
    torrent_creation_state_name = db.Column(db.String(40), default =
                                            str(DcpOperationState.NOT_DONE.name))
    torrent_creation_date        = db.Column(db.DateTime)
    #old_tasks = db.relationship('Task', back_'dcp', lazy='dynamic')
    task = db.relationship('Task', uselist=False,  backref='dcp')
    contentid =          db.Column(db.Integer)
    notifications = db.relationship('Notification', backref='dcp',
                                    lazy='dynamic')

    def from_dict(self, data):
        for field in ['valid', 'name', 'contentkind', 'size', 'torrent_hash', 'torrent_creation' ]:
            if field in data:
                setattr(self, field, data[field])

    def launch_task(self, name, description, *args, **kwargs):
        rq_job = current_app.task_queue.enqueue('app.tasks.' + name, self.id,
                                                timeout='12h',
                                                *args, **kwargs)
        print ("launch_task rq_job", rq_job)
        task = Task(id=rq_job.get_id(), name=name, description=description,
                    dcp=self)
        self.ingest_dcp_state = DcpOperationState.PENDING
        db.session.add(task)
        db.session.commit()
        return rq_job

    def add_notification(self, name, data):
        self.notifications.filter_by(name=name).delete()
        n = Notification(name=name, payload_json=json.dumps(data), dcp=self)
        db.session.add(n)
        return n


    def get_tasks_in_progress(self):
        return Task.query.filter_by(dcp=self, complete=False).all()

    def get_task_in_progress(self, name):
        return Task.query.filter_by(name=name, dcp=self,
                                    complete=False).first()

    @property
    def dcp_on_seed(self):
        return self.on_seed
    @dcp_on_seed.setter
    def dcp_on_seed(self, value):
        if value:
            self.on_seed = True
            self.on_seed_date = datetime.now()
        else:
            self.on_seed = False
            self.on_seed_date = None
        db.session.commit()


    @property
    def ingest_dcp_state(self):
        return DcpOperationState(self.ingest_dcp_state_value)
    @ingest_dcp_state.setter
    def ingest_dcp_state(self, value):
        self.ingest_dcp_state_value = value.value
        self.ingest_dcp_state_name = value.name
        if value == DcpOperationState.OK :
            self.ingest_dcp_date = datetime.now()
        db.session.commit()

    @property
    def hash_verification_state(self):
        return DcpOperationState(self.hash_verification_state_value)
    @hash_verification_state.setter
    def hash_verification_state(self, value):
        self.hash_verification_state_value = value.value
        self.hash_verification_state_name = value.name
        print ('Hash verification state : {}'.format(value))
        if value == DcpOperationState.OK :
            self.hash_verification_date = datetime.now()
        db.session.commit()

    @property
    def torrent_creation_state(self):
        return DcpOperationState(self.torrent_creation_state_value)
    @torrent_creation_state.setter
    def torrent_creation_state(self, value):
        self.torrent_creation_state_value = value.value
        self.torrent_creation_state_name = value.name
        if value == DcpOperationState.OK :
            self.torrent_creation_date = datetime.now()
        db.session.commit()

    def add_notification(self, name, data):
        self.notifications.filter_by(name=name).delete()
        n = Notification(name=name, payload_json=json.dumps(data), dcp=self)
        db.session.add(n)
        return n


class Notification(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), index=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    dcp_id = db.Column(db.Integer, db.ForeignKey('dcp.id'))
    timestamp = db.Column(db.Float, index=True, default=time)
    payload_json = db.Column(db.Text)

    def get_data(self):
        return json.loads(str(self.payload_json))


class Task(db.Model):
    id = db.Column(db.String(36), primary_key=True)
    name = db.Column(db.String(128), index=True)
    description = db.Column(db.String(128))
    complete = db.Column(db.Boolean, default=False)
    step = db.Column(db.String(32))
    progress = db.Column(db.Integer)
    state=db.Column(db.String(128))
    fail_msg = db.Column(db.Text, default='')
    start_date        = db.Column(db.DateTime)
    end_date          = db.Column(db.DateTime)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    task_id = db.Column(db.Integer, db.ForeignKey('dcp.id'))

    def __repr__(self):
        return '<Task {}: complete {} state {} >'.\
            format(self.name, self.complete, self.state)



    def get_rq_job(self):
        try:
            rq_job = rq.job.Job.fetch(self.id, connection=current_app.redis)
        except (redis.exceptions.RedisError, rq.exceptions.NoSuchJobError):
            return None
        return rq_job

    def get_progress(self):
        job = self.get_rq_job()
        print (job.meta)
        if job.meta.get('step'):
            self.step = job.meta.get('step')
        if job.meta.get('fail_msg'):
            self.fail_msg = job.meta.get('fail_msg')

        db.session.commit()
        return job.meta.get('progress', 0) if job is not None else 100

    def from_dict(self, data ):
        for field in ['name', 'description', 'complete', 'step', 'progress',
                     'state', 'fail_msg', 'start_date', 'end_date']:
            if field in data:
                if field in ['state','step']:
                    setattr(self, field, data[field].name)
                else:
                    setattr(self, field, data[field])
                if field=='progress' and data[field] == 0:
                    self.start_date = datetime.now()
                if field=='progress' and data[field] == 100:
                    self.end_date = datetime.now()


class TaskSchema(ma.ModelSchema):
    class Meta:
        model = Task

class DcpSchema(ma.ModelSchema):
    class Meta:
        model = Dcp
    # Smart hyperlinking
    _links = ma.Hyperlinks({
        'collection': ma.URLFor('ingestapi.get_dcps'),
        'self': ma.URLFor('ingestapi.get_dcp', id='<id>'),
        'short_check_status': ma.URLFor('ingestapi.get_short_check_status', id='<id>'),
    })
    #task_progress = fields.String( attribute='task.complete')

    seeders = fields.String(missing="coucou")
    task = fields.Nested(TaskSchema, many=False)
    movie = fields.Nested('MovieSchema', many=False, exclude=("dcps", ) )

class MovieSchema(ma.ModelSchema):
    class Meta:
        model = Movie
    # Smart hyperlinking
    _links = ma.Hyperlinks({
        'self': ma.URLFor('ingestapi.get_movie', id='<id>'),
    })
    dcps = fields.Nested(DcpSchema, many=True)
    distributor = fields.Nested('DistributorSchema', many=False, exclude=("movies", ) )

class UserSchema(ma.ModelSchema):
    class Meta:
        model = User
        exclude = ( 'tasks', 'password_hash', 'last_seen')

    role = fields.Function(lambda obj: UserRole(obj.role).name)
    distributor = fields.Nested('DistributorsSchema', many=False, exclude=('user',))
    exhibitor = fields.Nested('ExhibitorSchema', many=False, exclude=('user',))

    _links = ma.Hyperlinks({        'self': ma.URLFor('ingestapi.get_user', id='<id>'),
    })



class DistributorSchema(ma.ModelSchema):
    class Meta:
        model = Distributor

    # Smart hyperlinking
    _links = ma.Hyperlinks({
        'self': ma.URLFor('ingestapi.get_distributor', id='<id>'),
    })
    many = False
    movies = fields.Nested(MovieSchema, many=True, exclude=('dcps','distributor'))
    user = fields.Nested(UserSchema, many=False)

class DistributorsSchema(DistributorSchema):
    class Meta:
        exclude = ('movies', )

class NotificationSchema(ma.ModelSchema):
    class Meta:
        model = Notification

    data = fields.Function(lambda obj: obj.get_data())



class ExhibitorSchema(ma.ModelSchema):
    class Meta:
        model = Exhibitor
    # Smart hyperlinking
    _links = ma.Hyperlinks({
        'self': ma.URLFor('ingestapi.get_exhibitor', id='<id>'),
    })
    user = fields.Nested(UserSchema, many=False)

class ExhibitorsSchema(ExhibitorSchema):
    class Meta:
        exclude = ('users', )
    # Smart hyperlinking
    _links = ma.Hyperlinks({
        'self': ma.URLFor('ingestapi.get_exhibitor', id='<id>'),
    })
