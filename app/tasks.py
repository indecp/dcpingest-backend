#
# -*- coding: utf-8 -*-
# -*- Mode: Python -*-
# vim:si:ai:et:sw=4:sts=4:ts=4

import json
import sys
import time
import os
import os.path
import queue
import subprocess
import random
from datetime import datetime
from enum import IntEnum
from flask import render_template
from rq import get_current_job
from app import create_app, db, TDCPB_HOSTS
from app.models import User, Task, DcpOperationState, Dcp


from tdcpblib.tdcpb_checks import TdcpbCheckThread, tdcpb_contentkind
from tdcpblib.tdcpb_checks import tdcpb_dump, is_encrypted
from tdcpblib.ingest import IngestThread
from tdcpblib.common import TdcpbException
from tdcpblib.torrent_meta import TorrentMeta
from tdcpblib import search_dcp

app = create_app()
app.app_context().push()

import time


def _set_task_user_progress(progress):
    job = get_current_job()
    if job:
        print(progress)
        job.meta['progress'] = progress
        job.save_meta()
        task = Task.query.get(job.get_id())
        task.user.add_notification('task_progress', {'task_id': job.get_id(),
                                                     'progress': progress})
        if progress >= 100:
            task.complete = True
        db.session.commit()


def _set_dcp_task_progress(**kwargs):
    app.logger.info( "_set_dcp_task_progress : {}".format(kwargs))
    job = get_current_job()
    if job:
        print (job)
        progress = kwargs['progress']
        task = Task.query.get(job.get_id())
        job.meta['progress'] = progress
        task.progress  = int(progress)
        if 'state' in kwargs:
            task.dcp.ingest_dcp_state = kwargs['state']
            job.meta['state'] = kwargs['state'].name
        if 'step' in kwargs:
            job.meta['step']=  kwargs['step'].name

        if 'fail_msg' in kwargs:
            job.meta['fail_msg'] = kwargs['fail_msg']
        task.dcp.add_notification('task_progress', {'task_id': job.get_id(),
                                                     'progress': progress})
        if progress >= 100:
            kwargs['complete'] = True
            kwargs['progress'] = 100
        task.from_dict(kwargs)
        job.save_meta()
        db.session.commit()

def example(task):
    seconds = random.randrange(10,30,1)
    _set_task_user_progress(0)
    for i in range(seconds):
        _set_task_user_progress(100* i / seconds)
        time.sleep(1)
    _set_task_user_progress(100)
    return seconds

def check_dcp_short(user_id, dcp_id):
    try:
        print('Starting task')
        _set_dcp_task_progress(0)
        seconds=30
        for i in range(seconds):
            print(i)
            _set_dcp_task_progress(100* i / seconds)
            time.sleep(1)
        _set_dcp_task_progress(100)
        print('Task completed')
    except:
        _set_dcp_task_progress(100)
        app.logger.error('Unhandled exception', exc_info=sys.exc_info())



def task_start_seed(dcp_id):
    dcp = Dcp.query.get(dcp_id)
    app.logger.info('start seed for {}'.format(dcp.name))
    _set_dcp_task_progress( progress= 0, state = DcpOperationState.ON_PROGRESS)

    print(app.config['SEEDERS'])
    torrent_file= os.path.join(
                        app.config['TORRENT_DIR'],
                        '{}.torrent'.format(dcp.name))
    for seeder in app.config['SEEDERS'].split(';'):
        login, ipa = seeder.split('@')
        command =  [ "transmission-remote",
                ipa,
                "-n",
                login,
                "-a",
                torrent_file]
        process = subprocess.run(command)

    _set_dcp_task_progress( progress = 100,  state = DcpOperationState.OK)


def task_check_dcp_on_seed(user_id, dcp_id):
    dcp = Dcp.query.get(dcp_id)
    app.logger.info('start dcp search for {}'.format(dcp.name))
    job = get_current_job()
    _set_task_user_progress(progress = 0 )
    task = Task.query.get(job.get_id())
    seeders = []
    task.user.add_notification('dcp_on_seed', {'dcp_id': dcp_id,
                                               'progress':0})

    dcp.on_seed_active = True
    for seeder in app.config['SEEDERS'].split(';'):
        login, ipa = seeder.split('@')
        user, password = login.split(':')
        try:
            res = search_dcp(
                dcp.name,
                ipa,
                user,
                password)
        except TdcpbException as err:
            app.logger.error("Torrent client error: {}".format(err))
            _set_task_user_progress(progress = 100 )
            return -1

        if res and res['status'] == 'seeding' :
            dcp.on_seed_active =   dcp.on_seed_active &  True
        else:
            dcp.on_seed_active =   dcp.on_seed_active &  False
        print (res)
        if res:
            data = {}
            data['ipa'] = ipa
            data['host'] = TDCPB_HOSTS[ipa]
            data['status'] = res['status']
            data['name'] = res['name']
            data['hash'] = res['hash']
            data['progress'] = res['progress']
            data['error'] = res['error']
            data['errorString'] = res['errorString']
            data['progress'] = res['progress']
            data['date_active'] = "{}".format(res['date_active'])
            data['date_added'] = "{}".format(res['date_added'])
            data['date_done'] = "{}".format(res['date_done'])
            seeders.append(data)


    app.logger.info("{}".format(seeders))

    task.user.add_notification('dcp_on_seed', {'dcp_id': dcp_id,
                                               'progress':100,
                                                'seeders':seeders})

    db.session.commit()
    _set_task_user_progress(progress = 100 )

def ingest_dcp(dcp_id, copy_dcp, dcp_source_path=None):
    try:
        dcp = Dcp.query.get(dcp_id)
        app.logger.info('start ingest_dcp for {}'.format(dcp.name))
        _set_dcp_task_progress(progress= 0, state = DcpOperationState.ON_PROGRESS)
        if not os.path.exists(app.config['TORRENT_DIR']):
            _msg = "Torent folder {} not found"\
                    .format(app.config['TORRENT_DIR'])
            raise TdcpbException(_msg)
        if not os.path.exists(app.config['DCP_PATH']):
            _msg = "destination folder {} not found"\
                    .format(app.config['dcp_path'])
            raise tdcpbexception(_msg)
        if not copy_dcp:
            dcp_source_path = os.path.join(
                app.config['DCP_PATH'],
                dcp.name)
            if not os.path.exists(dcp_source_path):
                _msg = "dcp source {} not found"\
                        .format(dcp_source_path)
                raise TdcpbException(_msg)
        tqueue = queue.Queue()
        thread = IngestThread(
            tqueue,
            dcp_source_path,
            app.config['TORRENT_DIR'],
            copy_dcp,
            app.config['DCP_PATH'])
        thread.start()
        while True :
            try:
                msg = tqueue.get(block=False)
            except queue.Empty:
                pass
            else:
                print ('msg: ', msg)
                if 'exception' in msg:
                    exc_type, exc_value, exc_trace =  msg['exception']
                    raise TdcpbException(exc_value)
            step, progress = thread.get_progress()
            app.logger.info(" dcp_ingest {}: {} {}".format(dcp.name, step ,progress))
            _set_dcp_task_progress(progress = progress,
                           state = DcpOperationState.ON_PROGRESS,
                           step =  step)
            thread.join(1)
            if thread.isAlive():
                continue
            else:
                break
        try:
            msg = tqueue.get(block=False)
        except queue.Empty:
            pass
        else:
            if 'exception' in msg:
                exc_type, exc_value, exc_trace =  msg['exception']
                raise TdcpbException(exc_value)
    except TdcpbException as err:
        _set_dcp_task_progress(progress= 100, state = DcpOperationState.FAIL,
                           fail_msg = "{}".format(err))
        return -1
    except:
        _set_dcp_task_progress(progress = 100, state = DcpOperationState.FAIL)
        app.logger.error('Unhandled exception', exc_info=sys.exc_info())
        return -1
    step, progress = thread.get_progress()
    #
    # Read Torrent Meta info
    #
    torrent_file = os.path.join(app.config['TORRENT_DIR'],
                                '{}.torrent'.format(dcp.name))
    try:
        tinfo = TorrentMeta.info(torrent_file)
    except:
        _set_dcp_task_progress(progress = 100, state = DcpOperationState.FAIL,
                           step = step)
        app.logger.error('Unhandled exception', exc_info=sys.exc_info())
        return -1
    else:
        dcp.size = tinfo['size']
        dcp.torrent_hash = tinfo['hash']
        dcp.torrent_creation_date = tinfo['creation_date']
   #
    # Read DCP meta data: content kind encryption
    #
    dcp_path= os.path.join(app.config['DCP_PATH'], dcp.name)
    try:
        dcp.contentkind = tdcpb_contentkind(dcp_path)
    except TdcpbException as err:
        app.logger.err(err)
    try:
        data_dcp=tdcpb_dump(dcp_path)
    except:
        app.logger.err('error in tdcpb_dump for {}'.format(dcp.name))
    else:
        encrypted = is_encrypted(data_dcp)
        if encrypted != dcp.encrypted:
            dcp.encrypted = encrypted
    db.session.commit()

    #
    # Start seeding on torrent client
    #
    for seeder in app.config['SEEDERS'].split(';'):
        login, ipa = seeder.split('@')
        command =  [ "transmission-remote",
                ipa,
                "-n",
                login,
                "-a",
                torrent_file]
        process = subprocess.run(command)
        _set_dcp_task_progress( progress = 100,  state = DcpOperationState.OK,
                           step=step)
    return 0


def hash_verification(dcp_id):
    try:
        dcp = Dcp.query.get(dcp_id)
        app.logger.info('star DCP hash verification for {}'.format(dcp.name))
        _set_dcp_task_progress(progress = 0, state = DcpOperationState.ON_PROGRESS)
        dcp_path= os.path.join(app.config['DCP_PATH'], dcp.name)
        if not os.path.exists(dcp_path):
            _msg = "DCP path {} not found"\
                    .format(dcp_path)
            raise TdcpbException(_msg)

        tqueue = queue.Queue()
        thread = TdcpbCheckThread(tqueue, dcp_path , 'long')
        thread.start()
        while True :
            try:
                msg = tqueue.get(block=False)
            except queue.Empty:
                pass
            else:
                if 'exception' in msg:
                    exc_type, exc_value, exc_trace =  msg['exception']
                    raise TdcpbException(exc_value)
            thread.join(1)
            step, progress = thread.get_progress()
            _set_dcp_task_progress(progress = progress, step = step)
            if thread.isAlive():
                continue
            else:
                break
        try:
            msg = tqueue.get(block=False)
        except queue.Empty:
            pass
        else:
            if 'exception' in msg:
                exc_type, exc_value, exc_trace =  msg['exception']
                raise TdcpbException(exc_value)
            if 'isCheckvalid' in msg:
                if msg['isCheckvalid']:
                    app.logger.info("Hash Check OK for {}"\
                                    .format(dcp.name))
                    _set_dcp_task_progress( progress = 100,  state = DcpOperationState.OK)
                else:
                    msg= "Hash Check FAILED for {}".format(dcp.name)
                    app.logger.errror(msg)
                    _set_dcp_task_progress(
                        progress = 100,
                        state =  DcpOperationState.FAIL,
                        fail_msg =  msg,
                    )
    except TdcpbException as err:
        _set_dcp_task_progress(progress= 100, state = DcpOperationState.FAIL,
                           fail_msg = "{}".format(err))
    except:
        _set_dcp_task_progress( progress = 100, state = DcpOperationState.FAIL)
        app.logger.error('Unhandled exception', exc_info=sys.exc_info())
    else:
        step, progress = thread.get_progress()
        _set_dcp_task_progress( progress = 100,  state = DcpOperationState.OK,
                           step=step)

def torrent_creation(dcp_id):
    _sleep_duration = 10
    def _set_dcp_task_progress(progress, state):
        print ('_set_dcp_task_progress in torrent_creation')
        job = get_current_job()
        task = Task.query.get(job.get_id())
        job.meta['progress'] = progress
        job.meta['state'] = state.name
        task.dcp.torrent_creation_state = state
        if progress == 100:
            task.complete = True
        job.save_meta()
        db.session.commit()

    try:
        print('Starting task')
        _set_dcp_task_progress(0, DcpOperationState.ON_PROGRESS)
        time.sleep(_sleep_duration)
        _set_dcp_task_progress(25, DcpOperationState.ON_PROGRESS)
        time.sleep(_sleep_duration)
        _set_dcp_task_progress(50, DcpOperationState.ON_PROGRESS)
        time.sleep(_sleep_duration)
        _set_dcp_task_progress(75 , state = DcpOperationState.ON_PROGRESS)
        time.sleep(_sleep_duration)
        _set_dcp_task_progress(100,  state = DcpOperationState.OK)
        print('Task completed')
    except:
        _set_dcp_task_progress(100, state = DcpOperationState.FAIL)
        app.logger.error('Unhandled exception', exc_info=sys.exc_info())

def task_scan_dcp_folder(*args, **kwargs):
    dcp_path= os.path.join(app.config['DCP_PATH'])
    app.logger.info('start scan DCP folder {}'.format(dcp_path))
    _set_task_user_progress( progress= 0)
    job = get_current_job()
    task = Task.query.get(job.get_id())
    dcps_name = []
    for dcp in Dcp.query.filter_by(valid=True).all():
        if dcp.name not in os.listdir(dcp_path):
            _msg = 'Dcp {} is no more in Dcp folder'\
                            .format(dcp.name)
            app.logger.info(_msg)
            dcp.valid = False
            dcp.ingest_dcp_state = DcpOperationState.FAIL
            task.fail_msg=_msg


    for item in os.listdir(dcp_path):
        if not os.path.isdir(os.path.join(dcp_path,item)):
                 continue
        try:
            data_dcp = tdcpb_dump(os.path.join(dcp_path,item))
        except TdcpbException as err:
            app.logger.info('{} is not a DCP '.format(item))
            continue
        except:
            _msg = "Unexpected error: {}".format(sys.exc_info())
            raise tdcpbexception(_msg)
        else:
            if not Dcp.query.filter_by(name = item).first():
                app.logger.info('DCP {} added in DB'.format(item))
                new_dcp = Dcp(name =  item)
                new_dcp.ingest_dcp_state = DcpOperationState.NOT_DONE
                db.session.add(new_dcp)
                dcps_name.append(item)

    _set_task_user_progress(progress = 100 )
    task.complete = True
    db.session.commit()
    return 1
