# -*- coding: utf-8 -*-
# -*- Mode: Python -*-

import requests
from app.exceptions import IngestAutoException
from flask import current_app

class Content(object):
    NET_API_URL = 'http://localhost:5001/api'

    @classmethod
    def get(self,id):
        app = current_app._get_current_object()
        url= '{}/{}/{}'.format(self.NET_API_URL,'contents',id)
        r = requests.get(url)
        if r.status_code == 200 :
            return r.json()
        else:
            _err = 'error in {}. status code {}'.\
               format(url ,r.status_code)
            app.logger.error(_err)
            raise IngestAutoException(_err)

