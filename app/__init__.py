import logging
from logging.handlers import SMTPHandler, RotatingFileHandler
import os
from flask import Flask, request, current_app
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate
from flask_cors import CORS

from redis import Redis
import rq
from config import Config


db = SQLAlchemy()
ma = Marshmallow()
migrate = Migrate()

def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)
    CORS(app)
    db.init_app(app)
    print ("DATABASE_URL: ",app.config['SQLALCHEMY_DATABASE_URI'])
    ma.init_app(app)
    migrate.init_app(app, db)

    print ("REDIS_URL: ",app.config['REDIS_URL'])
    app.redis = Redis.from_url(app.config['REDIS_URL'])
    app.task_queue = rq.Queue('ingestauto-tasks', connection=app.redis)
    app.registry = rq.registry.FinishedJobRegistry('ingestauto-tasks', connection=app.redis)
    app.rq_job = None
    from app.errors import bp as errors_bp
    app.register_blueprint(errors_bp)


    from app.main import bp as main_bp
    app.register_blueprint(main_bp)

    #from app.api import bp as api_bp
    #app.register_blueprint(api_bp, url_prefix='/api')

    from app.ingestapi import bp as ingestapi_bp
    app.register_blueprint(ingestapi_bp, url_prefix='/ingestapi')

    print ("DCP PATH = {}".format(app.config['DCP_PATH']))
    if not app.debug and not app.testing:
        if app.config['MAIL_SERVER']:
            auth = None
            if app.config['MAIL_USERNAME'] or app.config['MAIL_PASSWORD']:
                auth = (app.config['MAIL_USERNAME'],
                        app.config['MAIL_PASSWORD'])
            secure = None
            if app.config['MAIL_USE_TLS']:
                secure = ()
            mail_handler = SMTPHandler(
                mailhost=(app.config['MAIL_SERVER'], app.config['MAIL_PORT']),
                fromaddr='no-reply@' + app.config['MAIL_SERVER'],
                toaddrs=app.config['ADMINS'], subject='Microblog Failure',
                credentials=auth, secure=secure)
            mail_handler.setLevel(logging.ERROR)
            app.logger.addHandler(mail_handler)

    if app.config['LOG_TO_STDOUT']:
        stream_handler = logging.StreamHandler()
        stream_handler.setLevel(logging.INFO)
        app.logger.addHandler(stream_handler)
    else:
        if not os.path.exists('logs'):
            os.mkdir('logs')
        file_handler = RotatingFileHandler('logs/ingestauto.log',
                                           maxBytes=2**30, backupCount=10)
        file_handler.setFormatter(logging.Formatter(
            '%(asctime)s %(levelname)s: %(message)s '
            '[in %(pathname)s:%(lineno)d]'))
        file_handler.setLevel(logging.INFO)
        app.logger.addHandler(file_handler)

    app.logger.setLevel(logging.INFO)
    app.logger.info('Ingest auto startup')


    return app


from app import models
from app.hosts import TDCPB_HOSTS
