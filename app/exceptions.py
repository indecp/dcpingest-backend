# -*- coding: utf-8 -*-
# -*- Mode: Python -*-

from flask import current_app

class IngestAutoException( Exception ):
    def __init__(self, message, status_code=None):
        if status_code is not None:
            self.status_code = status_code
        self.message = message

    def to_dict(self):
        rv = dict()
        rv[u'message'] = self.message
        return rv

    def __repr__(self):
        return u'<IngestAutoException> {}'.format(self.message.encode('utf-8'))

    def __str__(self):
        return u'<IngestAutoException> {}'.format(self.message)


