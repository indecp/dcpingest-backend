#
# -*- coding: utf-8 -*-
# -*- Mode: Python -*-
# vim:si:ai:et:sw=4:sts=4:ts=4


TDCPB_HOSTS = {
  'localhost'   : 'localhost',
  '10.10.10.75' : 'chucky',
  '10.10.10.100': 'godzilla',
  '10.10.10.101': 'moussaka',
  '10.10.10.102': 'smaug',
  '10.10.10.103': 'gizmo',
  '10.10.10.104': 'kingkong',
  '10.10.10.175': 'greenlantern'
}


