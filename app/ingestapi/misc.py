import json
from flask import jsonify, request, url_for, make_response
from flask_login import current_user
from app.ingestapi import bp
from tdcpblib.disk_usage import disk_usage
from tdcpblib.common import TdcpbException


from app import current_app
from app.models import User
from app.ingestapi.errors import bad_request, error_response

@bp.route('/misc/diskusage', methods=['GET'])
def diskusage():
    app = current_app._get_current_object()
    current_user = User.query.get(1)

    data = {}
    try:
        data= disk_usage(app.config['DCP_PATH'])._asdict()
        print ("disk_usage:", data)
        #print( json.dumps(data, sort_keys=True, indent=2))
    except TdcpbException as _err:
        app.logger.error(_err)
        return bad_request('Something goes wrong in disk usage')

    data['unit'] = 'GiB'
    data['path'] = app.config['DCP_PATH']
    data['free']  = round((1.0*data['free']) /2**30,3)
    data['total'] = round((1.0*data['total']) /2**30,3)
    data['used']  = round((1.0*data['used']) /2**30,3)
    data['percent_used']  = round( data['percent'],2)
    del (data['percent'])
    response = make_response()
    response.data = json.dumps(data, sort_keys=True, indent=2)
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response



@bp.route('/misc/parameters', methods=['GET'])
def parameters():
    app = current_app._get_current_object()
    current_user = User.query.get(1)
    data = {}
    data['dcp_path'] = app.config['DCP_PATH']
    data['torrent_path'] = app.config['TORRENT_DIR']
    response = make_response()
    response.data = json.dumps(data, sort_keys=True, indent=2)
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response

