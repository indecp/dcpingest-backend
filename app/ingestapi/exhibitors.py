import json
from flask import jsonify, request, url_for, make_response
from flask_login import current_user
from app import db
from app.models import Exhibitor, Dcp, DcpOperationState
from app.models import ExhibitorSchema
from app.ingestapi import bp
from app.ingestapi.errors import bad_request


@bp.route('/exhibitors/<int:id>', methods=['GET'])
def get_exhibitor(id):
    exhibitor = Exhibitor.query.get_or_404(id)

    schema = ExhibitorSchema(many=False)
    response = make_response()
    response.data = schema.dumps(exhibitor).data
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response

@bp.route('/exhibitors', methods=['GET'])
def get_exhibitors():
    exhibitors = Exhibitor.query.all()

    schema =ExhibitorSchema(many=True)
    response = make_response()
    response.data =  schema.dumps(exhibitors).data
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response

@bp.route('/exhibitors', methods=['POST'])
def create_exhibitor():
    data = request.get_json() or {}
    if 'name' not in data:
        return bad_request('must include exhibitor name')
    if Exhibitor.query.filter_by(name=data['name']).first():
        return bad_request('please use a different exhibitor name')
    exhibitor = Exhibitor()
    exhibitor.from_dict(data)
    db.session.add(exhibitor)
    db.session.commit()

    response = make_response()
    response.status_code = 201
    response.headers['Location'] = url_for('ingestapi.get_exhibitor', id=exhibitor.id)
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response



@bp.route('/exhibitors/<int:id>', methods=['PUT'])
def update_exhibitor(id):
    exhibitor = Exhibitor.query.get_or_404(id)
    data = request.get_json() or {}
    if 'name' in data and data['name'] != exhibitor.name and \
            Exhibitor.query.filter_by(name=data['name']).first():
        return bad_request('please use a different exhibitor name')
    exhibitor.from_dict(data)
    db.session.commit()
    schema = ExhibitorSchema()
    response = make_response()
    response.data = schema.dumps(exhibitor).data
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response

@bp.route('/exhibitors/<int:id>', methods=['DELETE'])
def delete_exhibitor(id):
    exhibitor = Exhibitor.query.get_or_404(id)
    db.session.delete(exhibitor)
    db.session.commit()

    response = make_response()
    response.status_code = 204
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response
