import json
from flask import jsonify, request, url_for, make_response

from app.ingestapi import bp
from app.ingestapi.errors import bad_request
from app.models import User, UserSchema


@bp.route('/login', methods=['POST'])
def login():
    data = request.get_json() or {}
    print (data)
    if 'login' not in data or 'password' not in data :
        return bad_request('must include login and password fields')
    user = User.query.filter_by(login=data['login']).first()
    if not user:
        return bad_request('Unknown login')
    token = user.get_token()
    schema = UserSchema()
    response = make_response()
    response.data = schema.dumps(user).data
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response
