import json
import requests
from flask import jsonify, request, url_for, make_response
from flask_login import current_user

from tdcpblib.disk_usage import disk_usage
from tdcpblib.common import TdcpbException


from app import current_app,db
from app.models import Dcp
from app.models import DcpSchema as LocalDcpSchema
from app.netapi import Content
from app.exceptions import IngestAutoException
from app.ingestapi import bp
from app.ingestapi.errors import error_404
from app.ingestapi.errors import bad_request
from marshmallow import Schema, fields

NET_API_URL="http://localhost:5001/api"


class  NetDcpSchema(Schema):
    id = fields.Integer()
    name = fields.Str()
    contentkind = fields.Str()
    encrypted = fields.Boolean()
    ingest_dcp_date = fields.DateTime()
    size = fields.Integer()
    torrent_hash = fields.Str()
    valid = fields.Boolean()

class ContentSchema(Schema):
    name = fields.Str()
    id = fields.Integer()
    dcps = fields.Nested(NetDcpSchema, many=True)
    local_dcps = fields.Nested(LocalDcpSchema, many=True)


@bp.route('/contents', methods=['GET'])
def get_contents():
    url= '{}/{}'.format(NET_API_URL,'contents')
    r = requests.get(url)
    if r.status_code == 200 :
        print(r.json())
        response = make_response()
        response.data = json.dumps(r.json(), sort_keys=True, indent=2)
        response.headers['Content-Type'] = ("application/json; charset=utf-8")
        return response
    else:
        return bad_request('error in {}. status code {}'.\
               format(url ,r.status_code) )

@bp.route('/contents/<int:id>', methods=['GET'])
def get_content(id):
    try:
        res = Content.get(id)
    except IngestAutoException as e:
        return error_404('{}'.format(e))
    res['local_dcps'] = Dcp.query.filter_by(contentid = id).all()

    print(res)
    schema = ContentSchema(many=False)
    response = make_response()
    response.data = schema.dumps(res).data
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response


    url= '{}/{}/{}'.format(NET_API_URL,'contents',id)
    r = requests.get(url)
    if r.status_code == 200 :
        print(r.json())
        response = make_response()
        response.data = json.dumps(r.json(), sort_keys=True, indent=2)
        response.headers['Content-Type'] = ("application/json; charset=utf-8")
        return response
    else:
        return bad_request('error in {}. status code {}'.\
               format(url ,r.status_code) )

@bp.route('/contents', methods=['POST'])
def post_contents():
    data = request.get_json() or {}
    print (data)
    url= '{}/{}'.format(NET_API_URL,'contents')
    r = requests.post(url, json=data)
    if r.status_code == 201 :
        print(r.json())
        response = make_response()
        response.data = json.dumps(r.json(), sort_keys=True, indent=2)
        response.headers['Content-Type'] = ("application/json; charset=utf-8")
        return response
    else:
        return bad_request('error in {}. status code {}'.\
               format(url ,r.status_code) )

@bp.route('/contents/<int:id>', methods=['DELETE'])
def delete_contents(id):
    url= '{}/{}/{}'.format(NET_API_URL,'contents',id)
    r = requests.delete(url)
    if r.status_code == 200 :
        print(r.json())
        response = make_response()
        response.data = json.dumps(r.json(), sort_keys=True, indent=2)
        response.headers['Content-Type'] = ("application/json; charset=utf-8")
        return response
    else:
        return bad_request('error in {}. status code {}'.\
               format(url ,r.status_code) )


