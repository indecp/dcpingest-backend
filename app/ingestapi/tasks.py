import json
from flask import jsonify, request, url_for, make_response
from flask_login import current_user

from tdcpblib.disk_usage import disk_usage
from tdcpblib.common import TdcpbException


from app import current_app,db
from app.models import User, Task, Notification
from app.models import TaskSchema, NotificationSchema
from app.ingestapi import bp
from app.ingestapi.errors import bad_request, error_response

@bp.route('/tasks', methods=['GET'])
def tasks():
    tasks = Task.query.\
            order_by(Task.start_date.desc()).all()
    schema = TaskSchema(many=True)
    response = make_response()
    response.data = schema.dumps(tasks).data
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response

@bp.route('/tasks/example', methods=['GET'])
def example():

    current_user = User.query.get(1)
    task = current_user.get_task_in_progress('example')
    data = {}

    response = make_response()
    if not task :
        task = current_user.launch_task('example', 'Example task')
        response.status_code = 202  # Accepted
        data['taskid'] = task.id
    else:
        response.status_code = 201  # OK
        data['progress']="on progress"
        data['taskid'] = task.id

    response.data = json.dumps(data)
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response

@bp.route('/tasks/notifications', methods=['GET'])
def notifications():

    current_user = User.query.get(1)
    #notifications = current_user.notifications.\
    notifications = Notification.query.\
    order_by(Notification.timestamp.asc())
    schema = NotificationSchema(many=True)
    response = make_response()
    response.data = schema.dumps(notifications).data
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response


@bp.route('/tasks/<string:id>', methods=['GET'])
def get_tasks(id):
    app = current_app._get_current_object()
    task = Task.query.get_or_404(id)
    schema = TaskSchema(many=False)
    response = make_response()
    response.data = schema.dumps(task).data
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response

@bp.route('/tasks/clear_tasks', methods=['GET'])
def clear_tasks():
    tasks = Task.query.filter_by(complete = False).all()
    for task in tasks:
        task.complete = True
    db.session.commit()

    tasks = Task.query.\
            order_by(Task.start_date.desc()).all()
    schema = TaskSchema(many=True)
    response = make_response()
    response.data = schema.dumps(tasks).data
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response

