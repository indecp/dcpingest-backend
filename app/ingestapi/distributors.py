import json
from flask import jsonify, request, url_for, make_response
from flask_login import current_user
from app import db
from app.models import Distributor, Dcp, DcpOperationState, Movie
from app.models import DistributorSchema, DistributorsSchema, MovieSchema, DcpSchema
from app.ingestapi import bp
from app.ingestapi.errors import bad_request


@bp.route('/distributors/<int:id>', methods=['GET'])
def get_distributor(id):
    distributor = Distributor.query.get_or_404(id)

    schema = DistributorSchema()
    response = make_response()
    response.data = schema.dumps(distributor).data
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response

@bp.route('/distributors', methods=['GET'])
def get_distributors():
    distributors = Distributor.query.all()

    schema =DistributorsSchema(many=True)
    response = make_response()
    response.data =  schema.dumps(distributors).data
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response

@bp.route('/distributors', methods=['POST'])
def create_distributor():
    data = request.get_json() or {}
    if 'name' not in data:
        return bad_request('must include distributor name')
    if Distributor.query.filter_by(name=data['name']).first():
        return bad_request('please use a different distributor name')
    distributor = Distributor()
    distributor.from_dict(data)
    db.session.add(distributor)
    db.session.commit()

    response = make_response()
    response.status_code = 201
    response.headers['Location'] = url_for('ingestapi.get_distributor', id=distributor.id)
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response



@bp.route('/distributors/<int:id>', methods=['PUT'])
def update_distributor(id):
    distributor = Distributor.query.get_or_404(id)
    data = request.get_json() or {}
    if 'name' in data and data['name'] != distributor.name and \
            Distributor.query.filter_by(name=data['name']).first():
        return bad_request('please use a different distributor name')
    distributor.from_dict(data)
    db.session.commit()
    schema = DistributorSchema()
    response = make_response()
    response.data = schema.dumps(distributor).data
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response



@bp.route('/distributors/<int:id>', methods=['DELETE'])
def delete_distributor(id):
    distributor = Distributor.query.get_or_404(id)
    db.session.delete(distributor)
    db.session.commit()

    response = make_response()
    response.status_code = 204
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response


@bp.route('/distributors/<int:id>/movies', methods=['GET'])
def get_distributor_movies(id):
    distributor = Distributor.query.get_or_404(id)
    movies = Movie.query.filter_by(distributor=distributor).\
            order_by(Movie.id.desc()).all()
    schema = MovieSchema(many=True)
    response = make_response()
    response.data = schema.dumps(movies).data
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response

@bp.route('/distributors/<int:id>/movies_newest', methods=['GET'])
def get_distributor_movies_newest(id):
    distributor = Distributor.query.get_or_404(id)
    movies = Movie.query.filter_by(distributor=distributor).\
            order_by(Movie.id.desc()).limit(5).all()
    schema = MovieSchema(many=True)
    response = make_response()
    response.data = schema.dumps(movies).data
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response

@bp.route('/distributors/<int:id>/dcps', methods=['GET'])
def get_distributor_dcps(id):
    distributor = Distributor.query.get_or_404(id)
    dcps = Dcp.query.join(Movie).filter(Movie.distributor == distributor).\
            order_by(Dcp.id.desc()).all()
    schema = DcpSchema(many=True)
    response = make_response()
    response.data = schema.dumps(dcps).data
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response


