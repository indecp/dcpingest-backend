import json
from flask import jsonify, request, url_for, make_response
from flask_login import current_user
from app import db
from app.models import User, Dcp, DcpOperationState, Movie, Distributor
from app.models import MovieSchema
from app.ingestapi import bp
from app.ingestapi.errors import bad_request


@bp.route('/movies/<int:id>', methods=['GET'])
def get_movie(id):
    response = make_response()
    movie = Movie.query.get_or_404(id)
    schema = MovieSchema(many=False)

    response = make_response()
    response.data = schema.dumps(movie).data
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response

@bp.route('/movies', methods=['GET'])
def get_movies():
    page = request.args.get('pageNumber', 1, type=int)
    per_page = min(request.args.get('pageSize', 50, type=int), 100)
    sort_order = request.args.get('sortOrder', 'ASC', type=str).lower()
    sort_field = request.args.get('sortField', 'id', type=str)
    query_filter = request.args.get('filter', '' , type=str)
    movies = Movie.query
    if query_filter:
        movies = Movie.query.filter(Movie.title.contains(query_filter))

    if sort_order == "asc":
        movies = movies.order_by(getattr(Movie, sort_field).asc())
    else:
        movies = movies.order_by(getattr(Movie, sort_field).desc())

    movies = movies.paginate(page ,per_page, False).items
    schema = MovieSchema(many=True)
    ret_data = {}
    ret_data['total'] = Movie.query.count()
    ret_data['payload'] = schema.dump(movies).data

    response = make_response()
    response.data =  json.dumps(ret_data)
    #response.data =  schema.dumps(movies).data
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response

@bp.route('/movies', methods=['POST'])
def create_movies():
    data = request.get_json() or {}
    print (data)
    if 'title' not in data:
        return bad_request('must include distributor name')
    response = make_response()
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    movie = Movie()
    movie.from_dict(data)
    if 'distributorid' in data:
        movie.distributor = Distributor.query.get(data['distributorid'])
    db.session.add(movie)
    db.session.commit()

    response = make_response()
    response.status_code = 201
    response.headers['Location'] = url_for('ingestapi.get_movie', id=movie.id)
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response


    return response


