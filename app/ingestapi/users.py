import json
from flask import jsonify, request, url_for, make_response
from flask_login import current_user
from app import db
from app.models import User, Dcp, DcpOperationState
from app.models import UserSchema
from app.ingestapi import bp
from app.ingestapi.errors import bad_request


@bp.route('/users/<int:id>', methods=['GET'])
def get_user(id):
    user = User.query.get_or_404(id)

    schema = UserSchema(many=False)
    response = make_response()
    response.data = schema.dumps(user).data
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response

@bp.route('/users', methods=['GET'])
def get_users():
    users = User.query.all()

    schema =UserSchema(many=True)
    response = make_response()
    response.data =  schema.dumps(users).data
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response

@bp.route('/users', methods=['POST'])
def create_user():
    data = request.get_json() or {}
    if 'login' not in data or 'email' not in data or 'password' not in data:
        return bad_request('must include username, email and password fields')
    if User.query.filter_by(login=data['login']).first():
        return bad_request('please use a different username')
    if User.query.filter_by(email=data['email']).first():
        return bad_request('please use a different email address')
    user = User()
    user.from_dict(data, new_user=True)
    db.session.add(user)
    db.session.commit()
    response = jsonify(user.to_dict())
    response.status_code = 201
    response.headers['Location'] = url_for('ingestapi.get_user', id=user.id)
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response



@bp.route('/users/<int:id>', methods=['PUT'])
def update_user(id):
    user = User.query.get_or_404(id)
    data = request.get_json() or {}
    if 'login' in data and data['login'] != user.login and \
            User.query.filter_by(login=data['login']).first():
        return bad_request('please use a different login')
    if 'email' in data and data['email'] != user.email and \
            User.query.filter_by(email=data['email']).first():
        return bad_request('please use a different email address')
    user.from_dict(data, new_user=False)
    db.session.commit()
    return jsonify(user.to_dict())
