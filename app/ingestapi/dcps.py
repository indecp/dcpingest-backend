import json
import shutil
import pprint
import os.path
from flask import jsonify, request, url_for, make_response
from flask_login import current_user
from app import db, current_app
from app.models import User, Dcp, DcpOperationState, Movie, Task
from app.models import Notification
from app.models import DcpSchema, NotificationSchema
from app.ingestapi import bp
from tdcpblib.tdcpb_checks import tdcpb_dump, tdcpb_contentkind, is_encrypted
from tdcpblib.common import TdcpbException

from rq import get_current_job
from app.ingestapi.errors import bad_request, error_response
from app.exceptions import IngestAutoException
from app.netapi import Content

@bp.route('/dcps/<int:id>', methods=['GET'])
def get_dcp(id):
    app = current_app._get_current_object()
    dcp = Dcp.query.get_or_404(id)
    task = dcp.get_task_in_progress('ingest_dcp')
    if task:
        print ('progress: {} '.format(task.get_progress()))
        task.progress = task.get_progress()
        db.session.commit()
    else:
        pass
    job = get_current_job()
    if job:
        print('Current job: %s' % (job.id,))
    dcp_schema = DcpSchema(many=False)
    response = make_response()
    response.data = dcp_schema.dumps(dcp).data
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response

@bp.route('/dcps/<int:id>/scan', methods=['GET'])
def scan_dcp(id):
    app = current_app._get_current_object()
    dcp = Dcp.query.get_or_404(id)

    dcp_path= os.path.join(app.config['DCP_PATH'], dcp.name)
    task = dcp.get_task_in_progress('ingest_dcp')
    try:
        data_dcp=tdcpb_dump(dcp_path)
    except TdcpbException as _err:
       app.logger.error(_err)
       dcp.ingest_dcp_state = DcpOperationState.FAIL
       db.session.commit()
       return error_response(404, "{}".format(_err))

    try:
        contentkind = tdcpb_contentkind(dcp_path)
    except TdcpbException as _err:
       app.logger.error(_err)
    else:
        if contentkind != dcp.contentkind:
            dcp.contentkind = contentkind
            db.session.commit()

    encrypted = is_encrypted(data_dcp)
    if encrypted != dcp.encrypted:
        dcp.encrypted = encrypted
        db.session.commit()



    dcp_schema = DcpSchema(many=False)
    response = make_response()
    response.data = json.dumps(data_dcp, sort_keys=True, indent=2)
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response

@bp.route('/dcps/<int:id>/toggle_validity', methods=['GET'])
def toggle_validity(id):
    app = current_app._get_current_object()
    dcp = Dcp.query.get_or_404(id)

    if dcp.valid:
        dcp.valid = False
    else:
        dcp.valid = True
    db.session.commit()

    dcp_schema = DcpSchema(many=False)
    response = make_response()
    response.data = dcp_schema.dumps(dcp).data
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response

@bp.route('/dcps/<int:id>/toggle_seed', methods=['POST'])
def toggle_seed(id):
    app = current_app._get_current_object()
    dcp = Dcp.query.get_or_404(id)
    dcp.dcp_on_seed = not dcp.dcp_on_seed
    response = make_response()
    if dcp.dcp_on_seed:
        tasks = dcp.get_tasks_in_progress()
        if not tasks :
            app.rq_job = dcp.launch_task('task_start_seed', 'Start seeding')
            response.status_code = 202  # Accepted
        else:
            return bad_request('a task is alreedy running for this DCP!')

    db.session.commit()

    dcp_schema = DcpSchema(many=False)
    response.data = dcp_schema.dumps(dcp).data
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response


@bp.route('/dcps/scan', methods=['GET'])
def scan_dcp_folder():
    app = current_app._get_current_object()
    app.logger.info('start folder scan')
    current_user = User.query.get(1)

    response = make_response()
    tasks = current_user.get_tasks_in_progress()
    if not tasks :
        rq_job = current_user.launch_task('task_scan_dcp_folder', 'Scan Dcp Folder')
        response.status_code = 202  # Accepted
    else:
        return bad_request('a task is alreedy running for this DCP!')
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response



@bp.route('/dcps/<int:id>', methods=['DELETE'])
def delete_dcp(id):
    dcp = Dcp.query.get_or_404(id)
    app = current_app._get_current_object()

    path_to_rm = os.path.normpath(os.path.join(app.config['DCP_PATH'],
                            dcp.name))

    if os.path.exists(path_to_rm):
        # Robustnness check
        if path_to_rm != os.path.normpath(app.config['DCP_PATH']):
            # TODO: handle errors on rmtree
            # TODO: path rm as task for long operations
            shutil.rmtree(path_to_rm)
        else:
            app.logger.error('Trying to remove DCP folder. Not a good idea at all')
    db.session.delete(dcp)

    db.session.commit()
    response = make_response()
    return response



@bp.route('/dcps', methods=['GET'])
def get_dcps():
    dcps = Dcp.query.\
            order_by(Dcp.id.desc()).all()
    schema = DcpSchema(many=True)
    response = make_response()
    response.data = schema.dumps(dcps).data
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response

@bp.route('/dcps', methods=['POST'])
def create_dcp():
    data = request.get_json() or {}
    print (data)
    response = make_response()
    dcp = Dcp()
    if 'dcp_path' in data and 'ingest' in data and data['ingest']:
        data['name'] = os.path.basename(data['dcp_path'])

        response.status_code = 202  # Accepted
    else :
        if 'name' not in data :
            return bad_request('must include name field')
        if Dcp.query.filter_by(name=data['name']).first():
            return bad_request('please use a different name')
        response.status_code = 201

    dcp.from_dict(data)
    if 'contentid' in data:
        try:
            Content.get(int(data['contentid']))
        except IngestAutoException as e:
            return bad_request("{}".format(e))
        else:
            dcp.contentid=data['contentid']
    db.session.add(dcp)
    db.session.commit()

    # task shall be laucnhed after DCP register in database
    if 'dcp_path' in data and 'ingest' in data and data['ingest']:
        tasks = dcp.get_tasks_in_progress()
        if not tasks :
            rq_job = dcp.launch_task('ingest_dcp', 'Ingest Dcp', copy_dcp = True, dcp_source_path=data['dcp_path'])
            dcp.ingest_auto_state = DcpOperationState.ON_PROGRESS
        else:
            return bad_request('a task is alreedy running for this DCP!')
    db.session.commit()

    dcp_schema = DcpSchema(many=False)

    response.data = dcp_schema.dumps(dcp).data
    response.headers['Location'] = url_for('ingestapi.get_dcp', id=dcp.id)
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response

@bp.route('/dcps/<int:id>/ingest', methods=['POST'])
def ingest_dcp(id):
    app = current_app._get_current_object()
    data = request.get_json() or {}
    #if 'name' not in data :
    #    return bad_request('must include name, contentkind fields')
    # TODO: replace first by one
    dcp = Dcp.query.get_or_404(id)

    response = make_response()

    tasks = dcp.get_tasks_in_progress()
    if not tasks :
        app.rq_job = dcp.launch_task('ingest_dcp', 'Ingest Dcp', copy_dcp =False, )
        dcp.ingest_auto_state = DcpOperationState.ON_PROGRESS
        response.status_code = 202  # Accepted
    else:
        return bad_request('a task is alreedy running for this DCP!')
    db.session.commit()
    dcp_schema = DcpSchema(many=False)

    print ("rq_job:  ", app.rq_job)
    response.data = dcp_schema.dumps(dcp).data
    response.headers['Location'] = url_for('ingestapi.get_dcp', id=dcp.id)
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response

@bp.route('/dcps/<int:id>/do_check_long', methods=['POST'])
def do_check_long_dcp(id):
    # data = request.get_json() or {}
    #if 'name' not in data :
    #    return bad_request('must include name, contentkind fields')
    # TODO: replace first by one
    dcp = Dcp.query.get_or_404(id)

    response = make_response()

    tasks = dcp.get_tasks_in_progress()
    if not tasks :
        rq_job = dcp.launch_task('hash_verification', 'Dcp long verification')
        response.status_code = 202  # Accepted
    else:
        return bad_request('a task is alreedy running for this DCP!')
    db.session.commit()
    dcp_schema = DcpSchema(many=False)

    response.data = dcp_schema.dumps(dcp).data
    response.headers['Location'] = url_for('ingestapi.get_dcp', id=dcp.id)
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response

@bp.route('/dcps/<int:id>/do_create_torrent', methods=['POST'])
def do_create_torrent(id):
    data = request.get_json() or {}
    #if 'name' not in data :
    #    return bad_request('must include name, contentkind fields')
    # TODO: replace first by one
    dcp = Dcp.query.get_or_404(id)

    response = make_response()

    tasks = dcp.get_tasks_in_progress()
    if not tasks :
        rq_job = dcp.launch_task('torrent_creation', 'Create torrent')
        response.status_code = 202  # Accepted
    else:
        return bad_request('a task is alreedy running for this DCP!')

    db.session.commit()
    dcp_schema = DcpSchema(many=False)

    response.data = dcp_schema.dumps(dcp).data
    response.headers['Location'] = url_for('ingestapi.get_dcp', id=dcp.id)
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response



@bp.route('/dcps/<int:id>/short_check_status', methods=['get'])
def get_short_check_status(id):
    current_user = User.query.get(1)
    dcp = dcp.query.get_or_404(id)
    response = make_response()
    if task:
        print ('a check status task is currently in progress')
        _dict = { 'progress': task.get_progress()}
        response.data = json.dumps(_dict)
    else:
        current_user.launch_task('check_dcp_short', 'check dcp short...', dcp.id)
        db.session.commit()
        response.data = json.dumps({ 'progress': 0})
    response.status_code = 202
    response.headers['location'] = url_for('ingestapi.get_dcp', id=dcp.id)
    response.headers['content-type'] = ("application/json; charset=utf-8")
    return response

@bp.route('/dcps/local_config', methods=['get'])
def get_local_config(id):
    local_config = {
     'dcp_storage_path': '/home/nicolas/data/DCP',
     'torrent_file_path': '/home/nicolas/data/torreents',
    }
    response = make_response()
    response.data = json.dumps('local_config')
    response.headers['content-type'] = ("application/json; charset=utf-8")
    return response




@bp.route('/users/<int:id>/followers', methods=['GET'])
def get_followers(id):
    user = User.query.get_or_404(id)
    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 10, type=int), 100)
    data = User.to_collection_dict(user.followers, page, per_page,
                                   'api.get_followers', id=id)
    return jsonify(data)


@bp.route('/users/<int:id>/followed', methods=['GET'])
def get_followed(id):
    user = User.query.get_or_404(id)
    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 10, type=int), 100)
    data = User.to_collection_dict(user.followed, page, per_page,
                                   'api.get_followed', id=id)
    return jsonify(data)


@bp.route('/dcps/<int:id>/search_dcp', methods=['GET'])
def search_dcp(id):
    user = User.query.get(1)
    app = current_app._get_current_object()
    dcp = Dcp.query.get_or_404(id)
    response = make_response()
    tasks = user.get_task_in_progress('task_check_dcp_on_seed')
    if not tasks :
        app.rq_job = user.launch_task('task_check_dcp_on_seed',
                                              'Search DCP on client', dcp.id)
        response.status_code = 202  # Accepted
    else:
        return bad_request('a task is alreedy running for this DCP!')
    db.session.commit()

    dcp_schema = DcpSchema(many=False)
    response.data = dcp_schema.dumps(dcp).data
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response


@bp.route('/dcps/<int:id>/search_dcp/notifications', methods=['GET'])
def search_dcp_nofifications(id):
    app = current_app._get_current_object()
    notification = Notification.query.filter_by(name='dcp_on_seed').\
    order_by(Notification.timestamp.desc()).\
    first()
    schema = NotificationSchema(many=False)
    response = make_response()
    response.data = schema.dumps(notification).data
    pprint.pprint(response.data)
    response.headers['Content-Type'] = ("application/json; charset=utf-8")
    return response
