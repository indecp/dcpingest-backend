# dcp ingest
Flask backend, root code based on flask mega tutorial.

## Install

install redis-server

### From git 
Backend install from git 
Go to a local dev dir (ie cd ~/dev)

```shell
git clone git@gitlab.com:indecp/dcpingest-backend.git
cd dcpingest-backend
python3 -m venv venv
. venv/bin/activate
pip install -r requirements.txt
```

## Populate server

ie add data to server
```shell
. ./bootstrap.sh
./ingscripts.sh populate samples/tuco_export.json
```

## Run backend server
```shell
. ./bootstrap.sh
flask run
```

Server will listen on flask default port AKA (5000)

## Start dcingest backend

[Marvel example](docs/example_marvel.md)

## API documentation

[API documentation](docs/rest_api.md)
