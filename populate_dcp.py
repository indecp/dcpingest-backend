from datetime import datetime, date
import dateutil.parser
import click
import json
import codecs
from flask.cli import with_appcontext
from app import create_app, db, cli

from app.models import Dcp, Movie,Task, User, Distributor, Exhibitor

import os
from app import db

app = create_app()

def import_users(json_data):
    for table in json_data:
        if 'user' in table :
            for item in table['user']:
                new_user = User(
                    id  = int(item['userid']),
                    login = item['login'],
                    email = item['email'],
                    role  = int(item['role']),
                    )
                db.session.add(new_user)
            db.session.commit()

def import_distributors(json_data):
    for table in json_data:
        if 'distributor' in table :
            for item in table['distributor']:
                new_distributor = Distributor(
                    id  = int(item['distributorid']))
                new_distributor.from_dict(item)
                try:
                    new_distributor.user = User.query.get(int(item['user']))
                except TypeError:
                    print ("Not a valid user ({}) for {}".format(item['user'], item))
                for movieid in item['movies']:
                    movie = Movie.query.get(int(movieid))
                    if not movie :
                        print ("No Movie {} for {}".format(movieid, item))
                        break
                    new_distributor.movies.append(movie)
                db.session.add(new_distributor)
            db.session.commit()

def import_exhibitors(json_data):
    for table in json_data:
        if 'exhibitor' in table :
            for item in table['exhibitor']:
                new_exhibitor = Exhibitor(
                    id  = int(item['exhibitorid']))
                new_exhibitor.user = User.query.get(int(item['user']))

                new_exhibitor.from_dict(item)
                db.session.add(new_exhibitor)
            db.session.commit()



def import_dcps(json_data):
    for table in json_data:
        if 'dcp' in table :
            for dcp in table['dcp']:
                new_dcp = Dcp (
                    id  = int(dcp['dcpid']),
                    contentkind = dcp['contentkind'],
                    size = dcp['size'],
                    name = dcp['name'],
                    valid = dcp['valid'],
                    torrent_hash = dcp['torrent_hash'],
                    torrent_creation_date =
                    dateutil.parser.parse((dcp['torrent_creation'])),
                    contentid = dcp['contentid'],
                    )
                db.session.add(new_dcp)
                print(new_dcp)
            db.session.commit()

def import_movies(json_data):
    for table in json_data:
        if 'movie' in table :
            for movie in table['movie']:
                new_movie = Movie(
                    id  = int(movie['movieid']),
                    valid  = movie['valid'],
                    title  = movie['title'],
                    original_title  = movie['original_title'],
                    releasedate  = datetime.strptime( movie['releasedate'], "%Y-%m-%d").date(),
                    nb_valid_ftr  = int(movie['nb_valid_ftr']),
                    nb_valid_tlr  = int(movie['nb_valid_tlr']),
                    )
                for dcpid in movie['dcps']:
                    dcp = Dcp.query.get(int(dcpid))
                    if not dcp :
                        print ("No Dcp for {}".format(movie))
                        break
                    new_movie.dcps.append(dcp)
                db.session.add(new_movie)
                #print (new_movie.to_dict())
            db.session.commit()


@click.command('populate_local')
@click.argument('jsonpath')
@with_appcontext
def populate_local(jsonpath):
    import os.path
    if os.path.exists('app.db'):
        import os
        os.remove('app.db')
    else:
        db.reflect()
        db.drop_all()
    if os.path.exists('migrations'):
        print ("migration dir found")
        import shutil
        shutil.rmtree('migrations')
    from flask_migrate import init, migrate, upgrade
    init()
    migrate()
    upgrade()
    with codecs.open(jsonpath, 'r', "utf-8") as infile:
        data = json.load(infile)
        import_users(data)
        import_dcps(data)


@click.command('populate')
@click.argument('jsonpath')
@with_appcontext
def populate(jsonpath):
    import os.path
    if os.path.exists('app.db'):
        import os
        os.remove('app.db')
    if os.path.exists('migrations'):
        print ("migration dir found")
        import shutil
        shutil.rmtree('migrations')
    from flask_migrate import init, migrate, upgrade
    init()
    migrate()
    upgrade()
    with codecs.open(jsonpath, 'r', "utf-8") as infile:
        data = json.load(infile)
        import_dcps(data)
        import_users(data)
        import_distributors(data)
        import_exhibitors(data)

@click.command('dump_movie')
@click.argument('movieid')
@with_appcontext
def dump_movie(movieid):
    """ dump movie info to file """
    _movie = Movie.query.get(int(movieid))
    if _movie:
        print (_movie.to_dict())

@click.command('clear_tasks')
@with_appcontext
def clear_tasks():
    """ dump movie info to file """
    tasks = Task.query.filter_by(complete = False).all()
    for task in tasks:
        task.complete = True
    db.session.commit()


app.cli.add_command(populate)
app.cli.add_command(populate_local)
app.cli.add_command(dump_movie)
app.cli.add_command(clear_tasks)
