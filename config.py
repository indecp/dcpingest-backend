import os
from dotenv import load_dotenv
basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(basedir, '.env'))


class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-guess'
    DATABASE_URI="mysql+pymysql://{}:{}@127.0.0.1:3307/{}".format(
                os.environ.get('INGEST_DBUSER'),
                os.environ.get('INGEST_DBPASSWORD'),
                os.environ.get('INGEST_DATABASE'))

    SQLALCHEMY_DATABASE_URI = DATABASE_URI or \
        'sqlite:///' + os.path.join(basedir, 'app.db')
    print(SQLALCHEMY_DATABASE_URI)
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    LOG_TO_STDOUT = os.environ.get('LOG_TO_STDOUT')
    MAIL_SERVER = os.environ.get('MAIL_SERVER')
    MAIL_PORT = int(os.environ.get('MAIL_PORT') or 25)
    MAIL_USE_TLS = os.environ.get('MAIL_USE_TLS') is not None
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')
    ADMINS = ['your-email@example.com']
    LANGUAGES = ['en', 'es']
    MS_TRANSLATOR_KEY = os.environ.get('MS_TRANSLATOR_KEY')
    ELASTICSEARCH_URL = os.environ.get('ELASTICSEARCH_URL')
    REDIS_URL = os.environ.get('REDIS_URL') or 'redis://'
    DCP_PATH= os.environ.get('DCP_PATH') or '/tmp/'
    TORRENT_DIR= os.environ.get('TORRENT_DIR') or '/tmp/'
    SEEDERS= os.environ.get('SEEDERS') or 'dummy:dummy@localhost'
    
