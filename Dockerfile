FROM python:3.6.8-stretch

#RUN adduser ingestauto

WORKDIR /app

COPY requirements.txt requirements.txt
#RUN python3 -m venv venv
#RUN venv/bin/pip install -r requirements.txt
RUN pip install pymysql
RUN pip install -r requirements.txt


COPY app app
COPY migrations migrations
COPY ingestauto.py config.py boot.sh populate_dcp.py ./

COPY samples/local_ingest.json samples/local_ingest.json
RUN chmod a+x boot.sh

COPY tdcpbtools-0.63.tar.gz ./
RUN tar -xvzf tdcpbtools-0.63.tar.gz \
  && cd tdcpbtools-0.63 \
  && python setup.py install

WORKDIR /app
ENV FLASK_APP populate_dcp
CMD ["flask", "populate_local", "samples/local_ingest.json"]

ENV FLASK_APP ingestauto

#CMD ["flask", "run"]
